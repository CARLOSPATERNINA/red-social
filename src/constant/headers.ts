import { HttpHeaders } from "@angular/common/http";

export class Headers {

public static getPOSTHeaders(token: string) {

    const httpHeaders = new HttpHeaders ({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      });

      return httpHeaders;
}

}
