// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  HOST: "http://18.189.21.84:5050",
  firebaseConfig: {
    apiKey:"AIzaSyARDXUqPLfeq0G9QwXv-nXKhKlMJeqyvPo",
    authDomain:"iniciofirebase-9a335.firebase.com",
    projectId:"iniciofirebase-9a335",
    storageBucket:"iniciofirebase-9a335.appspot.com",
    messagingSenderId:"108599302343",
    appId:"1:108599302343:web:1191268e8314fa1b2f06e2",
    measurementId: "G-0ZZFK7Y4EG"
  },
  production: false
};
