import { commentResponsetDTO } from './commentResponseDTO';
import { ReactionResponseDTO } from './reactionResponseDTO';
import { UserResponseDTO } from './userResponseDTO';

export interface PostResponseDTO{

    ID: number,
    CreatedAt: string
    UpdatedAt: string
    DeletedAt: null,
    image_url: string
    posted_text?: string
    user_id: number
    user: UserResponseDTO
    group: {
        ID: number,
        CreatedAt: string
        UpdatedAt: string
        DeletedAt: null
        name: string
        creator_id: number
    },
    comments: commentResponsetDTO[]
    reactions: ReactionResponseDTO[]
}
