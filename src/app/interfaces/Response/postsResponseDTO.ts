import { UserResponseDTO } from "./userResponseDTO"
import { commentResponsetDTO } from './commentResponseDTO';
import { ReactionResponseDTO } from './reactionResponseDTO';
import { PostResponseDTO } from "./postResponseDTO";

export interface PostsResponseDTO{
    count: number
    next: string
    results: PostResponseDTO[]
}


