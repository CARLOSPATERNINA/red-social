export interface UserResponseDTO{
  ID: number
  CreatedAt: string
  UpdatedAt: string
  DeletedAt: string
  name: string
  email: string
  password: string
  avatar: string
}
