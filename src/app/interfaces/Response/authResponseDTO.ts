export interface AuthResponseDTO {

  token: string,
  user: {
      ID: number,
      CreatedAt?: string,
      UpdatedAt?: string,
      DeletedAt?: null,
      name: string,
      email: string,
      avatar?: string
  }

}
