import { UserResponseDTO } from './userResponseDTO';
export interface GroupResponseDTO{
  ID: number
  CreatedAt: string
  UpdatedAt: string
  DeletedAt: null,
  name: string
  description: string
  imageUrl: string
  creator_id: number
  users:UserResponseDTO[]
}
