
export interface PostRequestDTO{
  posted_text: string
  image_url?: string
  user_id: number
}
