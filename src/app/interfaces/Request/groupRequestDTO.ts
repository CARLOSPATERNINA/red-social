export interface GroupRequestDTO{
  name: string
	description: string
	creator_id: number
}
