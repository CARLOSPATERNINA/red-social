import { PublicarComponent } from './components/publicar/publicar.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegistrarseComponent } from './components/registrarse/registrarse.component';
import { GruposComponent } from './components/grupos/grupos.component';
import { UserComponent } from './components/user/user.component';


const APP_ROUTES: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'publicacion', component: PublicarComponent},
  { path: 'registrarse', component: RegistrarseComponent},
  { path: 'grupos', component: GruposComponent},
  { path: 'usuarios', component: UserComponent},
  {path: '**', pathMatch: 'full', redirectTo:'login'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
