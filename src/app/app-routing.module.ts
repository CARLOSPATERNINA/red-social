import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicarComponent } from './components/publicar/publicar.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


 }
