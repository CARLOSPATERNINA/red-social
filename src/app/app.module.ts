import { APP_ROUTING } from './app.routes';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { PublicarComponent } from './components/publicar/publicar.component';
import { HttpClientModule } from '@angular/common/http';
import { RegistrarseComponent } from './components/registrarse/registrarse.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { HeaderComponent } from './components/shared/header/header.component';
import { AngularFireStorage, AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { CommentComponent } from './components/comment/comment.component';
import { GruposComponent } from './components/grupos/grupos.component';
import { UserComponent } from './components/user/user.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PublicarComponent,
    RegistrarseComponent,
    HeaderComponent,
    CommentComponent,
    GruposComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SweetAlert2Module,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    APP_ROUTING
  ],
  providers: [AngularFirestore, AngularFireStorage],
  bootstrap: [AppComponent]
})
export class AppModule { }
