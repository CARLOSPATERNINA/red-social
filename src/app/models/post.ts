import { User } from "./user";

export class Post {
  posted_text: string="";
  user_id:number=0;
  image_url?:string;
  user!:User;
  comments: Comment[]=[]
}
