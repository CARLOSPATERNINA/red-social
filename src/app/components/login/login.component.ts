import { User } from '../../models/user';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  registrarse: boolean = false;



  //Creando un objeto, creando una instancia de mi clase, inicializandolo

  formLogin: FormGroup = this.fb.group ({
    email: ['', [Validators.required]],
    password: ['', [Validators.required]]
  })


  //Creo una variable de tipo formBuilder en el constructor
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService:LoginService
  ) {  }

  ngOnInit(){

  }

  login() {
      const dataUser= this.formLogin.value;
      this.loginService.login(dataUser).subscribe( res => {
      localStorage.setItem("token", res.token);
      localStorage.setItem("userId", String(res.user.ID));
      localStorage.setItem("name", res.user.name);
      localStorage.setItem("email", res.user.email);
      localStorage.setItem("avatar", res.user.avatar!);
      this.router.navigateByUrl('/publicacion')
    }, err =>{
      Swal.fire({
        title: 'Error de Autenticación!',
        text: 'Usuario o contraseña Inválidas',
        icon: 'error',
        showConfirmButton: false,
        timer: 1500
      });

      this.formLogin.reset();
    })
  }


  btnRegistrarse(){

    this.registrarse = true;
    console.log(this.registrarse)
  }



}


