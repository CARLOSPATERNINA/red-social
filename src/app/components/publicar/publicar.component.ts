import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PostService } from '../../services/post.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { PostRequestDTO } from '../../interfaces/Request/postRequestDTO';
import { PostResponseDTO } from '../../interfaces/Response/postResponseDTO';
import { Observable, of } from 'rxjs';
import { Post } from '../../models/post';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-publicar',
  templateUrl: './publicar.component.html',
  styleUrls: ['./publicar.component.scss']
})
export class PublicarComponent implements OnInit {

  comentar: boolean = false;
  urlGlobal:string='';
  publicacionList!: PostResponseDTO[]
  comentario: string = "";
  imagen: string = "";
  publicaciones!: PostResponseDTO
  userId: string = ""
  url: Observable<any> = of("");
  post2!: PostResponseDTO

  event: any

  formPost: FormGroup = this.fb.group({
    contentPost: ['', [Validators.required]],
    imagePost: ['', [Validators.required]]
  })

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private postService: PostService,
    private storage: AngularFireStorage,
  ) {

  }

  ngOnInit() {
    this.comentar == false;
    this.userId = JSON.parse(localStorage.getItem('userId') || '{}')
    this.getPosts()
  }

  cerrarSesion() {
    this.router.navigateByUrl('login')
  }

  publicar() {
    const post: PostRequestDTO = {
      posted_text: this.formPost.value.contentPost,
      image_url: this.urlGlobal,
      user_id: +this.userId
    }

    this.postService.createPost(post).subscribe(res => {

      this.publicacionList.push(this.post2);
      Swal.fire({
        title: 'publicacion realizada',
        text: 'Su publicaciòn ha sido exitosa',
        icon: 'success',
        showConfirmButton: false,
        timer: 1700
      });
      this.formPost.reset();

    }, err => {
      console.log('No pudo hacer la peticion al servicio')
    })
  }

  getPosts() {
    this.postService.getPosts().subscribe(res => {
      console.log({res})
      this.publicacionList = res
    }, err => {
      console.log('Error en el servicio de getPOst')
    })
  }


  async onUpload(evt: any) {

    const archivo = evt.target.files[0];
    const partes = archivo.name.split('.');
    const extension = partes[partes.length - 1]
    console.log(archivo)
    const nombre = `${Date.now()}.${extension}`;
    console.log(nombre)
    const path = `upload/${nombre}`;
    const ref = this.storage.ref(path);
    const tarea = await this.storage.upload(path, archivo);


    let ref2 = await this.storage.ref(path);
    ref2.getDownloadURL().subscribe(val => this.urlGlobal = val);

  }

  getPost() {

  }

  viewComment() {
    this.comentar = true;
    console.log('el estado de comment es: ' + this.comentar)
  }


}
