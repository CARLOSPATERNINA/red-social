import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { GroupResponseDTO } from '../../interfaces/Response/groupResponseDTO';



@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.scss']
})
export class GruposComponent implements OnInit {

  constructor(private groupService: GroupService) { }

  contentbutton: string = "Suscribirme";
  userId: string = ""
  idGroup = 0;
  user_id = 0

  groupList!: GroupResponseDTO[]

  ngOnInit() {
    this.userId = JSON.parse(localStorage.getItem('userId') || '{}')
    this.getGroups();
  }

  getGroups() {
    this.groupService.getGroups().subscribe(res => {
      console.log(res)

      this.groupList = res
      this.user_id = +this.userId
      console
      for (let index = 0; index < this.groupList.length; index++) {
        this.idGroup = this.groupList[index].ID;
      }

    }, err => {
      console.log(err)
    })
  }


  addUserToGroup() {

    let addUserGroup={
      user_id: this.user_id,
      group_id: this.idGroup
    }

    this.groupService.addUserToGroup(addUserGroup).subscribe(res => {


      console.log( res );
      this.contentbutton = "suscrito"
    })
  }

}
