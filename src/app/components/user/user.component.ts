import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { UserResponseDTO } from '../../interfaces/Response/userResponseDTO';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  userList!: UserResponseDTO[]

  constructor(private userService:UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this.userService.getUsers().subscribe( res => {
     this.userList = res

    }, err => {

    })
  }

}
