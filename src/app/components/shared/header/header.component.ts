import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  username: string="";
  email: string="";
  avatar: string="";

  ngOnInit() {
    this.username = localStorage.getItem('name')!;
    this.email = localStorage.getItem('email')!;
    this.avatar =  localStorage.getItem('avatar')!;


  }



  cerrarSesion(){
    console.log('Se cerró la sesión')
    localStorage.clear();
    this.router.navigateByUrl('/')
  }

}
