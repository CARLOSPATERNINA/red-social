import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserRequestDTO } from 'src/app/interfaces/Request/userRequestDTO';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-registrarse',
  templateUrl: './registrarse.component.html',
  styleUrls: ['./registrarse.component.scss']
})
export class RegistrarseComponent implements OnInit {

  passwordAlert = false;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private router: Router   ) {

   }

   formRegister: FormGroup = this.fb.group ({
    name: ['', [Validators.required]],
    email: ['', [Validators.required]],
    password: ['', [Validators.required]],
    confirmPassword: ['', [Validators.required]],
  })

  ngOnInit(){
  }

  registrar() {
    console.log(this.formRegister.value)
    if(this.formRegister.get('password')?.value===this.formRegister.get('confirmPassword')?.value){

      const userData:UserRequestDTO = {
        name: this.formRegister.value.name,
        email: this.formRegister.value.email,
        password: this.formRegister.value.password
      }

      this.userService.createrUsers(userData).subscribe( res =>{
        console.log(res)
        Swal.fire({
          title: 'Usuario Registrado',
          text: 'Su registro ha sido exitoso',
          icon: 'success',
          showConfirmButton: false,
          timer: 1700
        });
        this.formRegister.reset();

      }, err =>{
       console.log(err)
        if  (err.status==400){
            Swal.fire({title: 'Error al registrarse',
            text: 'Su nombre y contraseña debe tener al menos 6 caracteres',
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
            });
        }  else if(err.status==500)  {
           Swal.fire({title: 'Error de conexion',
           text: 'Por favor intentelo más tarde',
           icon: 'error', showConfirmButton: false,
           timer: 1500
        })  }

      })
    } else {
      this.passwordAlert=true;
      this.formRegister.reset()
    }

    }



}
