import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PostRequestDTO } from '../interfaces/Request/postRequestDTO';
import { PostResponseDTO } from '../interfaces/Response/postResponseDTO';
import { PostsResponseDTO } from '../interfaces/Response/postsResponseDTO';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  token:string;
  constructor(private http:HttpClient) {
    this.token = localStorage.getItem('token')!;
   }

   getPOSTHeaders(token: string) {

    const httpHeaders = new HttpHeaders ({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    });

      return httpHeaders;
    }


  numPost="numPost"



  createPost(post:PostRequestDTO){
    return this.http.post(`${environment.HOST}/posts`, post, {headers: this.getPOSTHeaders(this.token)} )
  }

     getPostsbyUser(idUser:number){
      const headers = new HttpHeaders()
      .set('Authorization', localStorage.getItem('token') || '' )
      return this.http.get(`${environment.HOST}/posts/user/`+idUser,{ headers: this.getPOSTHeaders(this.token)})
    }

    getPostsPageable(){
      const headers = new HttpHeaders()
      .set('Authorization', localStorage.getItem('token') || '' )
      return this.http.get(`${environment.HOST}/posts/${this.numPost}`,{ headers: this.getPOSTHeaders(this.token)})
    }

    getPost(numPost: number){
      const headers = new HttpHeaders()
      .set('Authorization', localStorage.getItem('token') || '' )
      return this.http.get<PostResponseDTO>(`${environment.HOST}/posts/${this.numPost}`,{ headers: this.getPOSTHeaders(this.token)})
      }


      //-----------------------------

      getPosts(){
        const headers = new HttpHeaders()
          .set('Authorization', localStorage.getItem('token') || '' )
          return this.http.get<PostResponseDTO[]>(`${environment.HOST}/posts`, { headers: this.getPOSTHeaders(this.token)})
      }
}
