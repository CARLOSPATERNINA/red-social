import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserRequestDTO } from '../interfaces/Request/userRequestDTO';
import { UserResponseDTO } from '../interfaces/Response/userResponseDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  token:string;
  constructor(private http:HttpClient) {
    this.token = localStorage.getItem('token')!;
   }

   getPOSTHeaders(token: string) {

    const httpHeaders = new HttpHeaders ({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    });

      return httpHeaders;
    }

  getUsers(){
    const headers = new HttpHeaders()
    .set('Authorization', localStorage.getItem('token') || '' )

    return this.http.get<UserResponseDTO[]>(`${environment.HOST}/users`,{headers: this.getPOSTHeaders(this.token)})
  }


  createrUsers(user: UserRequestDTO){
      return this.http.post(`${environment.HOST}/users`,user)
  }
}
