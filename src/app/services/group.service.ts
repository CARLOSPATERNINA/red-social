import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { socialGroup } from '../models/group';
import { environment } from 'src/environments/environment';
import { GroupResponseDTO } from '../interfaces/Response/groupResponseDTO';
import { AddUserToGroup } from '../interfaces/Request/addUsertoGroup';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  token:string;
  constructor(private http:HttpClient) {
    this.token = localStorage.getItem('token')!;
  }

   getPOSTHeaders(token: string) {

    const httpHeaders = new HttpHeaders ({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    });

      return httpHeaders;
    }


  createGroup(socialGroup: socialGroup){

    return this.http.post(`${environment.HOST}/groups`,socialGroup, {headers: this.getPOSTHeaders(this.token)})
  }

  getGroups(){

    return this.http.get<GroupResponseDTO[]>(`${environment.HOST}/groups`,{headers: this.getPOSTHeaders(this.token)})
  }

  addUserToGroup(body: AddUserToGroup){
    return this.http.post<GroupResponseDTO[]>(`${environment.HOST}/groups/append`, body, {headers: this.getPOSTHeaders(this.token)})
  }


}
