import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Comments } from '../models/comments';

@Injectable({
  providedIn: 'root'
})
export class CommentService {



  constructor(private http:HttpClient) {   }



  createComment(comment:Comments, token: string){
    const headers = new HttpHeaders()
    .set('Authorization', localStorage.getItem('token') || '' )
    return this.http.post(`${environment.HOST}/comment`,comment, { headers })
  }
}
