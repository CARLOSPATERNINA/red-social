import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
import { AuthResponseDTO } from '../interfaces/Response/authResponseDTO';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }
  private _usuario!: User;




  login(user:User){
    const url = `${environment.HOST}/auth`;
    return this.http.post<AuthResponseDTO>(url,user);
  }










}
